//
//  MainTextField.swift
//  TipsterDemo
//
//  Created by Alex on 02.02.2022.
//

import UIKit

class MainTextField: UITextField {
    
    private let padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)

    enum State {
        case readyToBeNext
        case normal
        
        func backgroundColor() -> UIColor {
            switch self {
            case .normal:
                return DefaultColor.primaryGray
            case .readyToBeNext:
                return DefaultColor.primaryLightGray
            }
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        round(radius: 8, corners: .allCorners)
    }
    
    func configure(state: State = .normal, placeholder: String = "") {
        borderStyle = .none
        backgroundColor = state.backgroundColor()
        tintColor = .white
        textColor = .white
        font = DefaultFont.semiBold.size(17)

        attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: DefaultColor.placeholderGray,
            .font: DefaultFont.semiBold.size(17)
        ])
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
