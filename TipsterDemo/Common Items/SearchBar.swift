//
//  SearchBar.swift
//  TipsterDemo
//
//  Created by Alex on 03.02.2022.
//

import UIKit

class SearchBar: UISearchBar {

    func configure(placeholder: String = "") {
        searchTextField.borderStyle = .none
        backgroundColor = DefaultColor.primaryDarkGray
        searchTextField.backgroundColor = DefaultColor.primaryGray
        tintColor = .white
        searchTextField.textColor = .white
        searchTextField.font = DefaultFont.semiBold.size(17)
        barTintColor = DefaultColor.primaryDarkGray

        searchTextField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: DefaultColor.placeholderGray,
            .font: DefaultFont.semiBold.size(17)
        ])
    }
}

class SearchTextField: UITextField {
    
    private let padding = UIEdgeInsets(top: 0, left: 30, bottom: 0, right: 16)
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        round(radius: 10, corners: .allCorners)
    }
    
    func configure(placeholder: String = "") {
        borderStyle = .none
        backgroundColor = DefaultColor.primaryGray
        tintColor = .white
        textColor = .white
        font = DefaultFont.regular.size(16)

        attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [
            .foregroundColor: DefaultColor.placeholderGray,
            .font: DefaultFont.regular.size(16)
        ])
        self.addLeftImage()
    }
    
    private func addLeftImage() {
        let image = UIImage(named: "search_icon")
        let imageView = UIImageView(image: image)
        self.superview?.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        ConstraintManager.add(height: 14, width: 16, to: imageView)
        ConstraintManager.add(view: imageView, to: self, additionalView: self.superview, edge: .leading, constant: 7)
        ConstraintManager.add(view: imageView, to: self, additionalView: self.superview, edge: .centerY)
    }
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
