//
//  ActionSheetTableViewCell.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import UIKit

class ActionSheetTableViewCell: UITableViewCell {
    
    static let identifire = "ActionSheetTableViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var unselectedImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(model: BaseModel) {
        nameLabel.text = model.name
        if model.isSelected == true {
            selectedImageView.isHidden = false
            unselectedImageView.isHidden = true
        } else {
            selectedImageView.isHidden = true
            unselectedImageView.isHidden = false
        }
    }
}
