//
//  ActionSheetTitleTableViewCell.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import UIKit

class ActionSheetTitleTableViewCell: UITableViewCell {
    
    static let identifire = "ActionSheetTitleTableViewCell"
    
    @IBOutlet weak var titleLabel: UILabel!

    func configure(title: String) {
        titleLabel.text = title
    }
}
