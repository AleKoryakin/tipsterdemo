//
//  ActionSheetViewController.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import UIKit

protocol ActionSheetViewControllerDelegate: AnyObject {
    func actionSheetController(_ vc: ActionSheetViewController, didSelectItemAt index: Int)
}

class ActionSheetViewController: UIViewController {
    
    weak var delegate: ActionSheetViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomButton: MainAcceptButton!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    
    private var items: [BaseModel] = []
    private var titleText: String = ""
    private let defaultRowHeight: CGFloat = 56
    
    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUi()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        bottomButton.layoutIfNeeded()
        tableView.round(radius: 14, corners: .allCorners)
    }
    
    func configure(title: String, items: [BaseModel]) {
        self.items = items
        titleText = title
    }
    
    // MARK: - Actions
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss()
    }
    
    // MARK: - Helpers
    
    private func setupUi() {
        setHeight()
        configureTableView()
        bottomButton.configure(state: .cancel)
        fillLocalizedStrings()
    }
    
    private func setHeight() {
        heightConstraint.constant = CGFloat(items.count + 1) * defaultRowHeight
        view.layoutIfNeeded()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = DefaultColor.actionSheetGray
        tableView.register(UINib(nibName: ActionSheetTableViewCell.identifire, bundle: nil), forCellReuseIdentifier: ActionSheetTableViewCell.identifire)
        tableView.register(UINib(nibName: ActionSheetTitleTableViewCell.identifire, bundle: nil), forCellReuseIdentifier: ActionSheetTitleTableViewCell.identifire)
    }

    private func fillLocalizedStrings() {
        bottomButton.setTitle(Localize.Global.cancel, for: .normal)
    }
    
    private func dismiss() {
        dismiss(animated: true, completion: nil)
    }
}

extension ActionSheetViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view delegate / ds
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ActionSheetTitleTableViewCell.identifire, for: indexPath) as? ActionSheetTitleTableViewCell else {
                fatalError("Wrong cell type dequeued")
            }
            
            cell.configure(title: titleText)
            cell.selectionStyle = .none
            
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ActionSheetTableViewCell.identifire, for: indexPath) as? ActionSheetTableViewCell else {
                fatalError("Wrong cell type dequeued")
            }
            
            cell.configure(model: items[indexPath.row - 1])
            cell.setSelectedColor(DefaultColor.actionSheetGray)
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? 50 : defaultRowHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = DefaultColor.actionSheetGray
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.actionSheetController(self, didSelectItemAt: indexPath.row - 1)
        dismiss()
    }
}
