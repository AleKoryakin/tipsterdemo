//
//  TopErrorView.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import UIKit

class TopErrorView: UIView {

    @IBOutlet weak var mainLabel: UILabel!
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        round(radius: 16, corners: .allCorners)
    }
    
    func configure(message: String) {
        mainLabel.text = message
    }
}
