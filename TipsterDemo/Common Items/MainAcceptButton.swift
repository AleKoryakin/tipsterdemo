//
//  MainAcceptButton.swift
//  TipsterDemo
//
//  Created by Alex on 02.02.2022.
//

import UIKit

class MainAcceptButton: UIButton {
    
    enum State {
        case active
        case inactive
        case cancel
        
        func backgroundColor() -> UIColor {
            switch self {
            case .active:
                return DefaultColor.primaryBlue
            default:
                return DefaultColor.primaryGray
            }
        }
        
        func titleColor() -> UIColor {
            switch self {
            case .inactive:
                return DefaultColor.buttonInactiveGray
            default:
                return UIColor.white
            }
        }
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        round(radius: 16, corners: .allCorners)
    }

    func configure(state: State = .inactive) {
        backgroundColor = state.backgroundColor()
        setTitleColor(state.titleColor(), for: .normal)
        var font = DefaultFont.medium.size(18)
        switch state {
        case .inactive:
            self.isEnabled = false
        case .active:
            self.isEnabled = true
        case .cancel:
            self.isEnabled = true
            font = DefaultFont.regular.size(16)
        }
        titleLabel?.font = font
    }
}
