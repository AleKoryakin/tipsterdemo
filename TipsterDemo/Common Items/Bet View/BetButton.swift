//
//  BetButton.swift
//  TipsterDemo
//
//  Created by Alex on 17.02.2022.
//

import UIKit

class BetButton: UIButton {
    
    enum Style {
        case full
        case little
    }
    
    struct Model {
        var color: UIColor
        var secondColor: UIColor?
        var textColor: UIColor
        var imageName: String
        var topText: String
        var bottomText: String
    }

    var style: Style {
        return savedStyle
    }
    
//    private let subview: BetButtonSubview = .fromNib()
    private var savedStyle: Style = .full
    private var model: Model?
    
    func configure(style: Style, model: Model) {
        self.model = model
        savedStyle = style
        changePriority(style: style)
        if let secondColor = model.secondColor {
            backgroundColor = model.color
//            applyGradient(colours: [model.color, secondColor])
        } else {
            backgroundColor = model.color
        }
        setTitleColor(model.textColor, for: .normal)

        configureContent(model: model)
    }

    func changePriority(style: Style) {
        savedStyle = style
        switch style {
        case .full:
            setContentHuggingPriority(.defaultLow, for: .horizontal)
            centerTextAndImage(spacing: 10)
        default:
            setContentHuggingPriority(.defaultHigh, for: .horizontal)
            centerTextAndImage(spacing: 0)
        }
        configureContent(model: model!)
    }
    
    private func configureContent(model: Model) {
        switch style {
        case .full:
            setImage(UIImage(named: model.imageName), for: .normal)
            setAttributedTitle(attributedTitle(firstPart: model.topText, secondPart: model.bottomText, color: model.textColor), for: .normal)
        default:
            setImage(UIImage(named: model.imageName), for: .normal)
            setAttributedTitle(nil, for: .normal)
        }
    }
    
    private func attributedTitle(firstPart: String, secondPart: String, color: UIColor) -> NSAttributedString {
        
        let str = NSMutableAttributedString(string: firstPart, attributes: [
                                        .foregroundColor: color,
                                        .font: DefaultFont.medium.size(18)])
        str.append(NSAttributedString(string: "\n\(secondPart)", attributes: [
                            .foregroundColor: color,
                            .font: DefaultFont.regular.size(11)]))
        return str
    }
}


extension UIButton {

    func centerTextAndImage(spacing: CGFloat) {
        let insetAmount = spacing / 2
        let isRTL = UIView.userInterfaceLayoutDirection(for: semanticContentAttribute) == .rightToLeft
        if isRTL {
           imageEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
           titleEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
           contentEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: -insetAmount)
        } else {
           imageEdgeInsets = UIEdgeInsets(top: 0, left: -insetAmount, bottom: 0, right: insetAmount)
           titleEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: -insetAmount)
           contentEdgeInsets = UIEdgeInsets(top: 0, left: insetAmount, bottom: 0, right: insetAmount)
        }
    }
}
