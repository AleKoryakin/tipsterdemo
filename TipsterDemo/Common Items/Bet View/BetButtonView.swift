//
//  BetButtonView.swift
//  TipsterDemo
//
//  Created by Alex on 17.02.2022.
//

import UIKit

class BetButtonView: UIView {

    @IBOutlet var buttons: [BetButton]!
    
    func configure() {
        configureLeftButton()
        configureRightButton()
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
//        layout()
    }
    
    func layout() {
        buttons.forEach( {$0.round(radius: 16, corners: .allCorners)} )
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        guard let button = sender as? BetButton else { return }
        if button.style == .little {
            changePriority()
        }
    }

    private func configureLeftButton() {
        let model = BetButton.Model(color: DefaultColor.buttonGreen, textColor: .white, imageName: "temp_1stbet_icon", topText: "Поставить", bottomText: "Через Crocobet")
        buttons[0].configure(style: .full, model: model)
    }
    
    private func configureRightButton() {
        let model = BetButton.Model(color: DefaultColor.buttonYellowLight, secondColor: DefaultColor.buttonYellow, textColor: DefaultColor.buttonBlackTitle, imageName: "temp_2ndbet_icon", topText: "Поставить", bottomText: "С помощью Tipster Coins")
        buttons[1].configure(style: .little, model: model)
    }
    
    private func changePriority() {
        buttons.forEach( {$0.changePriority(style: $0.style == .full ? .little : .full)} )
//        UIView.animate(withDuration: 0.3) {
//
//        }
        
        UIView.animate(withDuration: 0.3) {
            self.layoutIfNeeded()
        } completion: { _ in
//            self.layout()
        }

    }
}
