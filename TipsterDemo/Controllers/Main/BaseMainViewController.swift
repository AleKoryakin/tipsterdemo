//
//  BaseMainViewController.swift
//  TipsterDemo
//
//  Created by Alex on 10.02.2022.
//

import UIKit

class BaseMainViewController: UIViewController {
    
    private let topView: MainTopView = .fromNib()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUi()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        topView.layoutIfNeeded()
    }
    
    func setupUi() {
        setMainViewColor(DefaultColor.primaryDarkGray)
        addTopView()
    }
    
    func addTopView() {
        topView.backgroundColor = .clear
        view.addSubview(topView)
        topView.translatesAutoresizingMaskIntoConstraints = false
        ConstraintManager.add(view: topView, to: view, edges: [.top, .trailing, .leading])
        ConstraintManager.add(height: 98, width: nil, to: topView)
        topView.configure()
    }
}
