//
//  MainTopView.swift
//  TipsterDemo
//
//  Created by Alex on 10.02.2022.
//

import UIKit

class MainTopView: UIView {
    
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet var topViews: [UIView]!
    @IBOutlet var topLabels: [UILabel]!
    @IBOutlet weak var userNameLabel: UILabel!
    
    private var tempGenerator = TempGenerator()
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        topViews.forEach( {$0.round(radius: $0.bounds.height / 2, corners: .allCorners)} )
        userImageView.round(radius: userImageView.bounds.height / 2, corners: .allCorners)
    }
    
    func configure() {
        userImageView.image = UIImage(named: tempGenerator.currentUser().imageName ?? "")
        userNameLabel.text = tempGenerator.currentUser().name
        topLabels[0].text = tempGenerator.currentUser().money
        topLabels[1].text = tempGenerator.currentUser().someCount
    }
    
    // MARK: - Actions
    
    @IBAction func notificationsButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func hamburgerButtonPressed(_ sender: Any) {
        
    }
}
