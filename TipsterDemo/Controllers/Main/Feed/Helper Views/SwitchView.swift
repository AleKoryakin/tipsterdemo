//
//  SwitchView.swift
//  TipsterDemo
//
//  Created by Alex on 14.02.2022.
//

import UIKit

protocol SwitchViewDelegate: AnyObject {
    func switchView(_ view: SwitchView, didSelectItemAt index: Int)
}

class SwitchView: UIView {
    
    weak var delegate: SwitchViewDelegate?

    @IBOutlet var mainLabels: [UILabel]!
    @IBOutlet var mainButtons: [UIButton]!
    @IBOutlet var rightViews: [UIView]!
    @IBOutlet var rightButtons: [UIButton]!
    
    private let selectedView = UIView()
    private var leadingSelectedViewConstraint: NSLayoutConstraint?
    private var trailingSelectedViewConstraint: NSLayoutConstraint?
    private var selectedIndex: Int = 0
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        rightViews.forEach( {$0.round(radius: 10, corners: .allCorners, borderWidth: 0.5, borderColor: UIColor(hex: 0x342B41).cgColor)} )
    }
    
    func configure(titles: [String]) {
        addSelectedView()
        select(duration: 0)
        fill(titles: titles)
    }

    // MARK: - Actions
    
    @IBAction func mainButtonPressed(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        let index = mainButtons.firstIndex(of: button) ?? 0
        if index != selectedIndex {
            selectedIndex = index
            select(duration: 0.3)
            delegate?.switchView(self, didSelectItemAt: index)
        }
    }
    
    @IBAction func rightButtonPressed(_ sender: Any) {
        
    }
    
    // MARK: - Helpers
    
    private func select(duration: Double) {
        mainLabels.forEach( {$0.textColor = DefaultColor.lightLabel} )
        mainLabels[selectedIndex].textColor = DefaultColor.primaryBlue
        animateSelectedView(duration: duration)
    }
    
    private func addSelectedView() {
        self.addSubview(selectedView)
        selectedView.translatesAutoresizingMaskIntoConstraints = false
        ConstraintManager.add(view: selectedView, to: self, edge: .bottom)
        ConstraintManager.add(height: 2, width: nil, to: selectedView)
        selectedView.backgroundColor = DefaultColor.primaryBlue
    }
    
    private func animateSelectedView(duration: Double) {
        if let leadingSelectedViewConstraint = leadingSelectedViewConstraint,
           let trailingSelectedViewConstraint = trailingSelectedViewConstraint {
            removeConstraints([leadingSelectedViewConstraint, trailingSelectedViewConstraint])
        }
        addExtremeConstraints()
        
        UIView.animate(withDuration: duration) {
            self.layoutSubviews()
        }
    }
    
    private func addExtremeConstraints() {
        leadingSelectedViewConstraint = selectedView.leadingAnchor.constraint(equalTo: mainLabels[selectedIndex].leadingAnchor)
        trailingSelectedViewConstraint = selectedView.trailingAnchor.constraint(equalTo: mainLabels[selectedIndex].trailingAnchor)
        addConstraints([leadingSelectedViewConstraint!, trailingSelectedViewConstraint!])
    }
    
    private func fill(titles: [String]) {
        for (index, element) in mainLabels.enumerated() {
            element.text = titles[index]
        }
    }
}
