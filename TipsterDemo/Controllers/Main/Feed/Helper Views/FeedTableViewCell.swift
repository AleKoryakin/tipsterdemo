//
//  FeedTableViewCell.swift
//  TipsterDemo
//
//  Created by Alex on 14.02.2022.
//

import UIKit

class FeedTableViewCell: UITableViewCell {

    static let identifire = "FeedTableViewCell"
    
    @IBOutlet weak var mainView: UIView!
    
    private let subview: BetEventView = .fromNib()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func layout() {
        mainView.round(radius: 16, corners: .allCorners)
        subview.layout()
    }

    func configure(type: BetEventViewType, model: BetEvent) {
        addSubview(type: type, model: model)
    }
    
    private func addSubview(type: BetEventViewType, model: BetEvent) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(subview)
        ConstraintManager.add(view: subview, to: mainView, edges: [.bottom, .top, .leading, .trailing])
        subview.configure(type: type, model: model)
    }
}
