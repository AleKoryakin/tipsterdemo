//
//  FeedViewController.swift
//  TipsterDemo
//
//  Created by Alex on 09.02.2022.
//

import UIKit

class FeedViewController: BaseMainViewController {
    
    @IBOutlet weak var switchContainerView: UIView!
    @IBOutlet weak var noContentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    private var switchView: SwitchView = .fromNib()
    private var selectedIndex: Int = 0
    private let totalCount: Int = 5

    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        switchView.layoutIfNeeded()
    }
    
    // MARK: - Action
    
    // MARK: - Helpers
    
    override func setupUi() {
        super.setupUi()
        addSwitchView()
        configureTableView()
        hideContentIfNeeded()
    }
    
    private func addSwitchView() {
        switchView.backgroundColor = .clear
        switchContainerView.addSubview(switchView)
        switchView.translatesAutoresizingMaskIntoConstraints = false
        ConstraintManager.add(view: switchView, to: switchContainerView, edges: [.top, .bottom, .trailing, .leading])
        switchView.configure(titles: ["Вся лента", "Подписки", "Лайки"])
        switchView.delegate = self
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = DefaultColor.primaryDarkGray
        tableView.register(UINib(nibName: FeedTableViewCell.identifire, bundle: nil), forCellReuseIdentifier: FeedTableViewCell.identifire)
    }
    
    private func hideContentIfNeeded(_ need: Bool = false) {
        tableView.isHidden = need
        noContentView.isHidden = !need
    }
    
    private func showCardScreen() {
        let vc = UserCardViewController.fromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension FeedViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view delegate / ds
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedIndex == 2 ? 0 : totalCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifire, for: indexPath) as? FeedTableViewCell else {
            fatalError("Wrong cell type dequeued")
        }
        
        cell.configure(type: .feed, model: TempGenerator().generateBetEvent())
        DispatchQueue.main.async {
            cell.layout()
        }
        
        cell.setSelectedColor(DefaultColor.primaryGray)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        DispatchQueue.main.async {
//            (cell as? FeedTableViewCell)?.layout()
//        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        showCardScreen()
    }
}

extension FeedViewController: SwitchViewDelegate {
    
    // MARK: - SwitchViewDelegate
    
    func switchView(_ view: SwitchView, didSelectItemAt index: Int) {
        selectedIndex = index
        hideContentIfNeeded(index == 2)
        tableView.reloadData()
    }
}
