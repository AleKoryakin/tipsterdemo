//
//  UserCardViewController.swift
//  TipsterDemo
//
//  Created by Alex on 16.02.2022.
//

import UIKit

class UserCardViewController: BaseMainViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var bottomView: UIView!

    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    // MARK: - Actions
    
    // MARK: - Helpers
    
    override func setupUi() {
        super.setupUi()
        addBottomView()
        configureTableView()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = DefaultColor.primaryDarkGray
        tableView.register(UINib(nibName: FeedTableViewCell.identifire, bundle: nil), forCellReuseIdentifier: FeedTableViewCell.identifire)
    }
    
    private func addBottomView() {
        let betView: BetButtonView = .fromNib()
        bottomView.addSubview(betView)
        betView.translatesAutoresizingMaskIntoConstraints = false
        ConstraintManager.add(view: betView, to: bottomView, edges: [.leading, .trailing, .bottom])
        ConstraintManager.add(height: 120, width: nil, to: betView)
        betView.configure()
//        DispatchQueue.main.async {
//            betView.layout()
//        }
    }
}

extension UserCardViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view delegate / ds
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FeedTableViewCell.identifire, for: indexPath) as? FeedTableViewCell else {
            fatalError("Wrong cell type dequeued")
        }
        
        cell.configure(type: .card, model: TempGenerator().generateBetEvent())
        DispatchQueue.main.async {
            cell.layout()
        }
        
        cell.selectionStyle = .none
        
        return cell
    }
}
