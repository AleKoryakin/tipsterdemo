//
//  ViewController.swift
//  TipsterDemo
//
//  Created by Alex on 18.01.2022.
//

import UIKit

class FastbetViewController: BaseMainViewController {
    
    @IBOutlet weak var swiperView: UIView!

    private let subview: SwiperView = .fromNib()
    private let tempGenerator = TempGenerator()

    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    // MARK: - Actions
    
    @IBAction func likeButtonPressed(_ sender: Any) {
        subview.animation(side: .right)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        subview.animation(side: .left)
    }
    
    // MARK: - Helpers
    
    override func setupUi() {
        super.setupUi()
        configureSwiperView()
    }
    
    private func configureSwiperView() {
        subview.translatesAutoresizingMaskIntoConstraints = false
        swiperView.addSubview(subview)
        let heightConstraint = subview.heightAnchor.constraint(equalTo: swiperView.heightAnchor)
        let widthConstraint = subview.widthAnchor.constraint(equalTo: swiperView.widthAnchor, constant: -32)
        let centerXConstraint = subview.centerXAnchor.constraint(equalTo: swiperView.centerXAnchor)
        swiperView.addConstraints([heightConstraint, widthConstraint, centerXConstraint])
        subview.configure()
        subview.layoutIfNeeded()
    }
}

