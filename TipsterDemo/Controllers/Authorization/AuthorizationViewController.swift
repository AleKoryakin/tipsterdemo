//
//  AuthorizationViewController.swift
//  TipsterDemo
//
//  Created by Alex on 31.01.2022.
//

import UIKit

class AuthorizationViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var phoneImageView: UIImageView!
    @IBOutlet weak var phoneTextField: MainTextField!
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet var checkboxButtons: [UIButton]!
    @IBOutlet var ppLabels: [UILabel]!
    @IBOutlet var ppButtons: [UIButton]!
    @IBOutlet weak var ppView: UIView!
    
    private let subview: TopAuthorizationView = .fromNib()
    private var languages = Language.generateTempLanguages()

    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        phoneView.round(radius: 8, corners: .allCorners)
        bottomButton?.layoutIfNeeded()
        phoneTextField.layoutIfNeeded()
    }
    
    // MARK: - Actions
    
    @IBAction func bottomButtonPressed(_ sender: Any) {
        showVerificationCodeScreen()
    }
    
    @IBAction func ppButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func checkboxButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func phoneButtonPressed(_ sender: Any) {
        showPhoneCodeScreen()
    }
    
    // MARK: - Helpers
    
    override func setupUi() {
        super.setupUi()
        fillLocalizedStrings()
        addTopView()
        bottomButton?.configure()
        phoneTextField.configure(placeholder: Localize.AuthorizationViewController.phonePlaceholder)
    }
    
    private func addTopView() {
        subview.delegate = self
        subview.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(subview)
        ConstraintManager.add(view: subview, to: view, edges: [.top, .trailing, .leading])
        ConstraintManager.add(height: 200, width: nil, to: subview)
        subview.configure(title: Localize.AuthorizationViewController.title)
    }
    
    private func fillLocalizedStrings() {
        mainLabels?[0].text = Localize.AuthorizationViewController.mainLabelFirstPart
        mainLabels?[1].text = Localize.AuthorizationViewController.mainLabelSecondPart
        hintLabel.text = Localize.AuthorizationViewController.hint
        ppLabels.forEach( {$0.text = Localize.AuthorizationViewController.acceptTitle} )
        ppButtons[0].setTitle(Localize.AuthorizationViewController.privacyPolicy, for: .normal)
        ppButtons[1].setTitle(Localize.AuthorizationViewController.tos, for: .normal)
        bottomButton?.setTitle(Localize.AuthorizationViewController.bottomButtonTitle, for: .normal)
    }
    
    private func fullPhoneNumber() -> String {
        return "\(phoneLabel.text ?? "")\(phoneTextField.text ?? "")"
    }
    
    // MARK: - Transitios
    
    private func showPhoneCodeScreen() {
        if let vc = PhoneCodeViewController.fromStoryboard() as? PhoneCodeViewController {
            vc.delegate = self
            present(vc, animated: true, completion: nil)
        }
    }
    
    private func showLanguageList() {
        if let vc = ActionSheetViewController.fromStoryboard() as? ActionSheetViewController {
            vc.modalPresentationStyle = .overCurrentContext
            vc.configure(title: Localize.ActionSheetViewController.title, items: languages)
            vc.delegate = self
            present(vc, animated: true, completion: nil)
        }
    }
    
    private func showVerificationCodeScreen() {
        if let vc = VerificationCodeViewController.fromStoryboard() as? VerificationCodeViewController {
            vc.configure(phoneNumber: fullPhoneNumber())
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}

extension AuthorizationViewController: UITextFieldDelegate {
    
    // MARK: - Text field delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        bottomButton?.configure(state: result.count > 3 ? .active : .inactive)
        
        return true
    }
}

extension AuthorizationViewController: PhoneCodeViewControllerDelegate {
    
    // MARK: - PhoneCodeViewControllerDelegate
    
    func viewController(_ vc: PhoneCodeViewController, didSelect country: Country) {
        phoneImageView.image = country.getFlag()
        phoneLabel.text = country.dialCode
    }
}

extension AuthorizationViewController: TopAuthorizationViewDelegate {
    
    // MARK: - TopAuthorizationViewDelegate
    
    func viewDidPressLanguageButton(_ view: TopAuthorizationView) {
        showLanguageList()
    }
}

extension AuthorizationViewController: ActionSheetViewControllerDelegate {
    
    // MARK: - ActionSheetViewControllerDelegate
    
    func actionSheetController(_ vc: ActionSheetViewController, didSelectItemAt index: Int) {
        languages.forEach( {$0.isSelected = false} )
        languages[index].isSelected = true
        subview.changeLanguage(languages[index].subname ?? "RU")
    }
}
