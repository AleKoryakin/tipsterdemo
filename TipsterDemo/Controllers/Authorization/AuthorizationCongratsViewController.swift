//
//  AuthorizationCongratsViewController.swift
//  TipsterDemo
//
//  Created by Alex on 08.02.2022.
//

import UIKit

class AuthorizationCongratsViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var hintLabel: UILabel!
    @IBOutlet weak var nameTextField: MainTextField!
    
    private var name: String = ""
    private var tempWrongName = "Temp"

    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        bottomButton?.layoutIfNeeded()
        nameTextField.layoutIfNeeded()
    }
    
    // MARK: - Actions
    
    @IBAction func bottomButtonPressed(_ sender: Any) {
        start()
    }
    
    // MARK: - Helpers
    
    override func setupUi() {
        super.setupUi()
        fillLocalizedStrings()
        bottomButton?.configure()
        nameTextField.configure(placeholder: Localize.AuthorizationCongratsViewController.namePlaceholder)
    }
    
    private func fillLocalizedStrings() {
        mainLabels?[0].text = Localize.AuthorizationCongratsViewController.mainLabelText
        hintLabel.text = Localize.AuthorizationCongratsViewController.hint
        bottomButton?.setTitle(Localize.Global.start, for: .normal)
    }
    
    private func start() {
        if name == tempWrongName {
            TopMessagesManager().showTopMessage(message: Localize.AuthorizationCongratsViewController.topMessage)
        } else {
            showMainScreen()
        }
    }
    
    private func showMainScreen() {
        let vc = MainTabBarViewController.fromStoryboard()
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}

extension AuthorizationCongratsViewController: UITextFieldDelegate {
    
    // MARK: - Text field delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        name = result
        bottomButton?.configure(state: result.count > 3 ? .active : .inactive)
        
        return true
    }
}
