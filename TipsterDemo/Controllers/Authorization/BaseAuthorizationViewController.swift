//
//  BaseAuthorizationViewController.swift
//  TipsterDemo
//
//  Created by Alex on 31.01.2022.
//

import UIKit

class BaseAuthorizationViewController: UIViewController {
    
    @IBOutlet weak var mainContentView: UIView?
    @IBOutlet weak var mainViewConstraint: NSLayoutConstraint?
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint?
    @IBOutlet weak var bottomButton: MainAcceptButton?
    @IBOutlet var mainLabels: [UILabel]?

    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        setupUi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden), name:UIResponder.keyboardWillHideNotification, object: nil)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Helpers
    
    func setupUi() {
        setMainViewColor(DefaultColor.primaryDarkGray)
    }
    
    // MARK: - Keyboard
    
    @objc func keyboardWillBeHidden(aNotification: Notification) {
        animateContent(value: 0, top: false)
    }
    
    @objc func keyboardWillShow(aNotification: Notification) {
        let info = aNotification.userInfo!
        let kbSize: CGSize = ((info["UIKeyboardFrameEndUserInfoKey"] as? CGRect)?.size)!
        animateContent(value: kbSize.height, top: true)
    }
    
    private func animateContent(value: CGFloat, top: Bool) {
        let defaultBottomOffset: CGFloat = 21
        let bottomConstraintValue: CGFloat = top ? defaultBottomOffset + value : defaultBottomOffset
        let centerOffset = (DeviceSize.height - value) / 3.5
        let mainConstraintValue: CGFloat = top ? -centerOffset : 0
        
        bottomConstraint?.constant = bottomConstraintValue
        mainViewConstraint?.constant = mainConstraintValue
        
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
}
