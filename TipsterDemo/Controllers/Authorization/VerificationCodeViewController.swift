//
//  VerificationCodeViewController.swift
//  TipsterDemo
//
//  Created by Alex on 31.01.2022.
//

import UIKit

class VerificationCodeViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var topLabel: UILabel!
    @IBOutlet weak var resendButton: UIButton!
    @IBOutlet var textFields: [MainTextField]!
    @IBOutlet weak var resendLabel: UILabel!
    @IBOutlet weak var resendButtonBottomView: UIView!
    
    private var phoneNumber: String = ""
    private var activeIndex: Int = 0
    private var timer = Timer()
    private let defaultSecondsCount = 60
    private var secondsLeft = 60
    private var fullCode = ""
    
    private let tempWrongCode = "6666"
    
    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        bottomButton?.layoutIfNeeded()
        textFields.forEach( {$0.round(radius: 16, corners: .allCorners)} )
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func configure(phoneNumber: String) {
        self.phoneNumber = phoneNumber
    }
    
    // MARK: - Actions
    
    @IBAction func backButtonPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendButtonPressed(_ sender: Any) {
        secondsLeft = defaultSecondsCount
        setupTimer()
    }
    
    @IBAction func bottomButtonPressed(_ sender: Any) {
        sendCode()
    }
    
    // MARK: - Helpers
    
    override func setupUi() {
        super.setupUi()
        fillLocalizedStrings()
        bottomButton?.configure(state: .inactive)
        textFields.forEach { $0.delegate = self }
        configureTextField(index: activeIndex)
        activateTextField()
        setupTimer()
    }
    
    private func fillLocalizedStrings() {
        mainLabels?[0].text = Localize.VerificationCodeViewController.mainLabelFirstPart
        bottomButton?.setTitle(Localize.Global.continue, for: .normal)
        topLabel.text = Localize.VerificationCodeViewController.backButtonTitle
        resendButton.setTitle(Localize.VerificationCodeViewController.resendButtonTitle, for: .normal)
        mainLabels?[1].text = Localize.formatString(Localize.VerificationCodeViewController.mainLabelSecondPart, argument: phoneNumber)
        resendLabel.text = Localize.formatString(Localize.VerificationCodeViewController.resendText, argument: "01:00")
    }

    private func setNormalStateToAllTextFields() {
        textFields.forEach { $0.configure(state: .normal) }
    }
    
    private func configureTextField(index: Int) {
        setNormalStateToAllTextFields()
        if index < 3 {
            textFields[index + 1].configure(state: .readyToBeNext)
        }
    }
    
    private func goToNextTextFieldIfPossible() {
        let index = activeIndex + 1
        if index < 4 {
            activeIndex = index
            configureTextField(index: index)
            activateTextField()
        } else {
            deactivateTextField()
        }
    }
    
    private func activateTextField() {
        textFields[activeIndex].becomeFirstResponder()
    }
    
    private func deactivateTextField() {
        textFields[activeIndex].resignFirstResponder()
    }
    
    private func resetAllTextFields() {
        textFields.forEach { $0.text = nil }
        changeBottomButtonState()
    }
    
    private func changeBottomButtonState() {
        var isActive = true
        fullCode = ""
        for tf in textFields {
            if (tf.text ?? "").isEmpty {
                isActive = false
                break
            } else {
                fullCode.append(tf.text ?? "")
            }
        }
        bottomButton?.configure(state: isActive ? .active : .inactive)
    }
    
    private func sendCode() {
        if fullCode == tempWrongCode {
            TopMessagesManager().showTopMessage(message: Localize.VerificationCodeViewController.topMessage)
            resetAllTextFields()
        } else {
            showCongratsScreen()
        }
    }
    
    // MARK: - Timer
    
    private func setupTimer() {
        resendSubviewsVisible(isStop: false)
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc private func timerAction() {
        var seconds = "01:00"
        secondsLeft -= 1
        if secondsLeft > 0 {
            seconds = secondsLeft < 10 ? "00:0\(secondsLeft)" : "00:\(secondsLeft)"
        } else {
            stopTimer()
        }
        resendLabel.text = Localize.formatString(Localize.VerificationCodeViewController.resendText, argument: seconds)
    }
    
    private func stopTimer() {
        resendSubviewsVisible(isStop: true)
        timer.invalidate()
        timer = Timer()
    }
    
    private func resendSubviewsVisible(isStop: Bool) {
        resendLabel.isHidden = isStop
        resendButton.isHidden = !isStop
        resendButtonBottomView.isHidden = !isStop
    }
    
    // MARK: - Transitios
    
    private func showCongratsScreen() {
        let vc = AuthorizationCongratsViewController.fromStoryboard()
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension VerificationCodeViewController: UITextFieldDelegate {

// MARK: - Text field delegate

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if result.count > 0 {
            textField.text = result
            goToNextTextFieldIfPossible()
        }
        changeBottomButtonState()
        
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if (textField.text ?? "").isEmpty == false {
            textField.text = nil
        }
        if let tf = textField as? MainTextField {
            activeIndex = textFields.firstIndex(of: tf) ?? 0
            configureTextField(index: activeIndex)
        }
    }
}
