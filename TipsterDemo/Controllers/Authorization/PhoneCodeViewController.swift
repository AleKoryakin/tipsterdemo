//
//  PhoneCodeViewController.swift
//  TipsterDemo
//
//  Created by Alex on 31.01.2022.
//

import UIKit

protocol PhoneCodeViewControllerDelegate: AnyObject {
    func viewController(_ vc: PhoneCodeViewController, didSelect country: Country)
}

class PhoneCodeViewController: BaseAuthorizationViewController {
    
    weak var delegate: PhoneCodeViewControllerDelegate?
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var littleTopView: UIView!
    @IBOutlet weak var searchBar: SearchTextField!
    
    private var customCountriesCode: [String]?
    private var countries = [Country]()
    private var filteredList = [Country]()
    private lazy var callingCodes = { () -> [[String: String]] in
        let resourceBundle = Bundle(for: PhoneCodeViewController.classForCoder())
        guard let path = resourceBundle.path(forResource: "CallingCodes", ofType: "plist") else { return [] }
        return NSArray(contentsOfFile: path) as! [[String: String]]
    }()

    // MARK: - Vc life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        hideKeyboardWhenTappedAround()
        configureTableView()
        countries = unsortedCountries()
        tableView.reloadData()
        searchBar.configure(placeholder: Localize.Global.search)
        topView.backgroundColor = DefaultColor.primaryGray
        fillLocalizedStrings()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        searchBar.layoutIfNeeded()
        littleTopView.round(radius: littleTopView.bounds.height / 2, corners: .allCorners)
    }
    
    // MARK: - Actions
    
    @IBAction func closeButtonPressed(_ sender: Any?) {
        dismiss()
    }
    
    // MARK: - Helpers
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = DefaultColor.primaryDarkGray
        tableView.register(UINib(nibName: CountryTableViewCell.identifire, bundle: nil), forCellReuseIdentifier: CountryTableViewCell.identifire)
    }
    
    private func unsortedCountries() -> [Country] {
        let locale = Locale.current
        var unsortedCountries = [Country]()
        let countriesCodes = customCountriesCode == nil ? Locale.isoRegionCodes : customCountriesCode!
        
        for countryCode in countriesCodes {
            let displayName = (locale as NSLocale).displayName(forKey: NSLocale.Key.countryCode, value: countryCode)
            let countryData = callingCodes.filter { $0["code"] == countryCode }
            let country: Country
            
            if countryData.count > 0, let dialCode = countryData[0]["dial_code"] {
                country = Country(name: displayName!, code: countryCode, dialCode: dialCode)
            } else {
                country = Country(name: displayName!, code: countryCode)
            }
            unsortedCountries.append(country)
        }
        
        return unsortedCountries
    }
    
    private func filter(_ searchText: String) -> [Country] {
        filteredList.removeAll()
        
        countries.forEach({ (country) -> () in
            if country.name.count >= searchText.count {
                let result = country.name.compare(searchText, options: [.caseInsensitive, .diacriticInsensitive],
                                                  range: searchText.startIndex ..< searchText.endIndex)
                if result == .orderedSame {
                    filteredList.append(country)
                }
            }
        })
        
        return filteredList
    }
    
    private func dismiss() {
        dismiss(animated: true, completion: nil)
    }
    
    private func fillLocalizedStrings() {
        titleLabel.text = Localize.PhoneCodeViewController.title
    }
}

extension PhoneCodeViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Table view delegate / ds
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CountryTableViewCell.identifire, for: indexPath) as? CountryTableViewCell else {
            fatalError("Wrong cell type dequeued")
        }
        
        cell.setSelectedColor(DefaultColor.primaryDarkGray)
        cell.configure(model: countries[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = DefaultColor.primaryDarkGray
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.viewController(self, didSelect: countries[indexPath.row])
        dismiss()
    }
}

extension PhoneCodeViewController: UITextFieldDelegate {
    
    // MARK: - Text field delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let result = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) ?? string
        
        if result.count > 0 {
            countries = unsortedCountries()
            countries = filter(result)
        } else {
            countries = unsortedCountries()
        }
        tableView.reloadData()
        
        return true
    }
}
