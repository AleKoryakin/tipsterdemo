//
//  CountryTableViewCell.swift
//  TipsterDemo
//
//  Created by Alex on 03.02.2022.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    static let identifire = "CountryTableViewCell"
    
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var countryLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    func configure(model: Country) {
        countryLabel.text = "\(model.name) (\(model.dialCode ?? ""))"
        flagImageView.image = model.getFlag()
    }
}
