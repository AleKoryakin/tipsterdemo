//
//  TopAuthorizationView.swift
//  TipsterDemo
//
//  Created by Alex on 01.02.2022.
//

import UIKit

protocol TopAuthorizationViewDelegate: AnyObject {
    func viewDidPressLanguageButton(_ view: TopAuthorizationView)
}

class TopAuthorizationView: UIView {
    
    weak var delegate: TopAuthorizationViewDelegate?
    
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var languageView: UIView!
    @IBOutlet weak var languageImageView: UIImageView!
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        languageView.round(radius: 8, corners: .allCorners)
    }
    
    func configure(title: String) {
        layoutIfNeeded()
        mainLabel.text = title
    }
    
    func changeLanguage(_ language: String) {
        languageLabel.text = language
    }
    
    // MARK: - Actions
    
    @IBAction func languageButtonPressed(_ sender: Any) {
        delegate?.viewDidPressLanguageButton(self)
    }
}
