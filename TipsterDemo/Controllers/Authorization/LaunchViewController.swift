//
//  LaunchViewController.swift
//  TipsterDemo
//
//  Created by Alex on 31.01.2022.
//

import UIKit

class LaunchViewController: BaseAuthorizationViewController {
    
    @IBOutlet weak var gifImageView: UIImageView!
    
//    private var isAuthorized: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()

        let gifImage = UIImage.gifImageWithName("launch")
        gifImageView.image = gifImage
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.showFirstScreen()
        }
    }
    
    // MARK: - Helpers

    private func showFirstScreen() {
        if AutologinManager.isAuthorized == false {
            showAuthorizationScreen()
        } else {
            showMainScreen()
        }
    }
    
    private func showAuthorizationScreen() {
        let vc = AuthorizationViewController.fromStoryboard()
        let nc = UINavigationController(rootViewController: vc)
        nc.modalPresentationStyle = .fullScreen
        present(nc, animated: true, completion: nil)
    }
    
    private func showMainScreen() {
        let vc = MainTabBarViewController.fromStoryboard()
//        let nc = UINavigationController(rootViewController: vc)
        vc.modalPresentationStyle = .fullScreen
        present(vc, animated: true, completion: nil)
    }
}
