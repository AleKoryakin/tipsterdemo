//
//  ConstraintManager.swift
//  TipsterDemo
//
//  Created by Alex on 02.02.2022.
//

import Foundation
import UIKit

class ConstraintManager {
    
    enum Edge {
        case top
        case bottom
        case leading
        case trailing
        case centerX
        case centerY
    }
    
    class func add(view: UIView, to parrentView: UIView, edges: [Edge]) {
        edges.forEach {
            add(view: view, to: parrentView, edge: $0)
        }
    }
    
    class func add(view: UIView, to parrentView: UIView, additionalView: UIView? = nil, edge: Edge, constant: CGFloat = 0) {
        switch edge {
        case .top:
            (additionalView ?? parrentView).addConstraint(view.topAnchor.constraint(equalTo: parrentView.topAnchor, constant: constant))
        case .bottom:
            (additionalView ?? parrentView).addConstraint(view.bottomAnchor.constraint(equalTo: parrentView.bottomAnchor, constant: constant))
        case .leading:
            (additionalView ?? parrentView).addConstraint(view.leadingAnchor.constraint(equalTo: parrentView.leadingAnchor, constant: constant))
        case .trailing:
            (additionalView ?? parrentView).addConstraint(view.trailingAnchor.constraint(equalTo: parrentView.trailingAnchor, constant: constant))
        case .centerX:
            (additionalView ?? parrentView).addConstraint(view.centerXAnchor.constraint(equalTo: parrentView.centerXAnchor, constant: constant))
        case .centerY:
            (additionalView ?? parrentView).addConstraint(view.centerYAnchor.constraint(equalTo: parrentView.centerYAnchor, constant: constant))
        }
    }
    
    class func add(height: CGFloat?, width: CGFloat?, to view: UIView) {
        if let height = height {
            view.addConstraint(view.heightAnchor.constraint(equalToConstant: height))
        }
        if let width = width {
            view.addConstraint(view.widthAnchor.constraint(equalToConstant: width))
        }
    }
}
