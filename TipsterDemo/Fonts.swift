//
//  Fonts.swift
//  TipsterDemo
//
//  Created by Alex on 01.02.2022.
//

import Foundation
import UIKit

private let familyName = "NotoSans"

enum DefaultFont: String {
    case black = "Black"
    case extraBold = "ExtraBold"
    case bold = "Bold"
    case semiBold = "SemiBold"
    case medium = "Medium"
    case regular = "Regular"
    case condensed = "Condensed"
    case light = "CondensedLight"
    case extraLight = "ExtraCondensedExtraLight"
    case boldItalic = "BoldItalic"
    case italic = "Italic"
    
    func size(_ size: CGFloat) -> UIFont {
        if let font = UIFont(name: fullFontName, size: size) {
            return font
        }
        fatalError("Font '\(fullFontName)' does not exist.")
    }
    
    fileprivate var fullFontName: String {
        return rawValue.isEmpty ? familyName : familyName + "-" + rawValue
    }
}
