//
//  BetAbbreviatedInfoView.swift
//  TipsterDemo
//
//  Created by Alex on 12.02.2022.
//

import UIKit

class BetAbbreviatedInfoView: BaseEventSubview {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet var roundedViews: [UIView]!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet var partImageViews: [UIImageView]!
    @IBOutlet var partLabels: [UILabel]!
    @IBOutlet var bottomLabels: [UILabel]!
    @IBOutlet weak var xLabel: UILabel!
    
    override func configure(model: BetEvent) {
        titleLabel.text = model.subtitle
        dateLabel.text = model.date
        timeLabel.text = model.time
        for (index, item) in model.parts!.enumerated() {
            partImageViews[index].image = UIImage(named: item.imageName ?? "")
            partLabels[index].text = item.name
        }
        bottomLabels[0].text = "Рынок"
        bottomLabels[1].text = model.market?.name()
        bottomLabels[2].text = "Исход"
        bottomLabels[3].text = model.outcome
        numberLabel.text = "1 из 5"
    }
    
    func layout() {
        roundedViews[0].round(radius: 6, corners: .allCorners)
        roundedViews[1].round(radius: 6, corners: .allCorners)
        roundedViews[2].round(radius: 8, corners: .allCorners)
    }
}
