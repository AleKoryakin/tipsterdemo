//
//  BetEventStackSubview.swift
//  TipsterDemo
//
//  Created by Alex on 16.02.2022.
//

import UIKit

class BetEventStackSubview: BaseEventSubview {
    
    @IBOutlet weak var stackView: UIStackView!

    override func configure(model: BetEvent) {
        addSubview(model: TempGenerator().generateBetEvent())
        addSubview(model: TempGenerator().generateBetEvent())
    }
    
    private func addSubview(model: BetEvent) {
        let subview: BetAbbreviatedInfoView = .fromNib()
        subview.translatesAutoresizingMaskIntoConstraints = false
        ConstraintManager.add(height: 194, width: nil, to: subview)
        stackView.addArrangedSubview(subview)
        subview.configure(model: model)
        DispatchQueue.main.async {
            subview.round(radius: 12, corners: .allCorners)
        }
    }
}
