//
//  EventCollectionViewCell.swift
//  TipsterDemo
//
//  Created by Alex on 14.02.2022.
//

import UIKit

class EventCollectionViewCell: UICollectionViewCell {
    
    static let identifire = "EventCollectionViewCell"
    
    @IBOutlet weak var mainView: UIView!
    
    private let subview: BetAbbreviatedInfoView = .fromNib()
    
    func configure(model: BetEvent) {
        addSubview(model: model)
    }
    
    func layout() {
        mainView.round(radius: 12, corners: .allCorners)
        subview.layout()
    }
    
    private func addSubview(model: BetEvent) {
        subview.translatesAutoresizingMaskIntoConstraints = false
        mainView.addSubview(subview)
        ConstraintManager.add(view: subview, to: mainView, edges: [.bottom, .top, .leading])
        ConstraintManager.add(view: subview, to: mainView, edge: .trailing, constant: 0)
        subview.configure(model: model)
    }
}
