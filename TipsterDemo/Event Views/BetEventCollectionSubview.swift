//
//  BetEventCollectionSubview.swift
//  TipsterDemo
//
//  Created by Alex on 14.02.2022.
//

import UIKit

class BetEventCollectionSubview: BaseEventSubview {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bottomLabel: UILabel!
    
    private var model = BetEvent()
    private var totalCount = 5

    override func configure(model: BetEvent) {
        self.model = model
        configureCollectionView()
    }
    
    // MARK: - Helpers
    
    private func configureCollectionView() {
        collectionView.backgroundView = nil
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: EventCollectionViewCell.identifire, bundle: nil), forCellWithReuseIdentifier: EventCollectionViewCell.identifire)
    }
}

extension BetEventCollectionSubview: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Collection view delegate / ds
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return totalCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: EventCollectionViewCell.identifire, for: indexPath) as? EventCollectionViewCell else {
            fatalError("Wrong cell type dequeued")
        }
        
        cell.configure(model: TempGenerator().generateBetEvent())
        DispatchQueue.main.async {
            cell.layout()
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.bounds.width - 41, height: collectionView.bounds.height)
    }
}
