//
//  LocalizedString.swift
//  TipsterDemo
//
//  Created by Alex on 02.02.2022.
//

import Foundation

struct Localize {
    static func string(_ key: String) -> String {
        return NSLocalizedString(key, comment: "comment")
    }
    
    static func formatString(_ key: String, argument: String) -> String {
        return String(format: key, argument)
    }
    
    // MARK: Global
    struct Global {
        static var search: String { Localize.string("Global.Search.Placeholder") }
        static var cancel: String { Localize.string("Global.Cancel.Title") }
        static var `continue`: String { Localize.string("Global.Continue.Title") }
        static var start: String { Localize.string("Global.Start.Title") }
    }
    
    // MARK: AuthorizationViewController
    struct AuthorizationViewController {
        static var title: String { Localize.string("AuthorizationViewController.Title") }
        static var mainLabelFirstPart: String { Localize.string("AuthorizationViewController.MainLabel.FirstPart") }
        static var mainLabelSecondPart: String { Localize.string("AuthorizationViewController.MainLabel.SecondPart") }
        static var hint: String { Localize.string("AuthorizationViewController.Hint") }
        static var acceptTitle: String { Localize.string("AuthorizationViewController.AcceptTitle") }
        static var privacyPolicy: String { Localize.string("AuthorizationViewController.PrivacyPolicy") }
        static var tos: String { Localize.string("AuthorizationViewController.TermsOfService") }
        static var bottomButtonTitle: String { Localize.string("AuthorizationViewController.BottomButton.Title") }
        static var phonePlaceholder: String { Localize.string("AuthorizationViewController.PhonePlaceholder") }
    }
    
    // MARK: PhoneCodeViewController
    struct PhoneCodeViewController {
        static var title: String { Localize.string("PhoneCodeViewController.Title") }
    }
    
    // MARK: ActionSheetViewController
    struct ActionSheetViewController {
        static var title: String { Localize.string("ActionSheetViewController.Title") }
    }
    
    // MARK: VerificationCodeViewController
    struct VerificationCodeViewController {
        static var backButtonTitle: String { Localize.string("VerificationCodeViewController.BackButton.Title") }
        static var mainLabelFirstPart: String { Localize.string("VerificationCodeViewController.MainLabel.FirstPart") }
        static var mainLabelSecondPart: String { Localize.string("VerificationCodeViewController.MainLabel.SecondPart") }
        static var resendText: String { Localize.string("VerificationCodeViewController.ResendText") }
        static var resendButtonTitle: String { Localize.string("VerificationCodeViewController.ResendButton.Title") }
        static var topMessage: String { Localize.string("VerificationCodeViewController.TopMessage") }
    }
    
    // MARK: AuthorizationCongratsViewController
    struct AuthorizationCongratsViewController {
        static var namePlaceholder: String { Localize.string("AuthorizationCongratsViewController.NamePlaceholder") }
        static var mainLabelText: String { Localize.string("AuthorizationCongratsViewController.MainLabel") }
        static var hint: String { Localize.string("AuthorizationCongratsViewController.Hint") }
        static var topMessage: String { Localize.string("AuthorizationCongratsViewController.TopMessage") }
    }
 }
