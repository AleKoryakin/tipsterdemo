//
//  Language.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import Foundation

class Language: BaseModel {
    
    var subname: String?
    
    init(name: String, isSelected: Bool = false, subname: String) {
        super.init(name: name, isSelected: isSelected)
        self.name = name
        self.isSelected = isSelected
        self.subname = subname
    }
    
    class func generateTempLanguages() -> [Language] {
        return [Language(name: "Русский", isSelected: true, subname: "RU"), Language(name: "English", subname: "EN"), Language(name: "Georgian", subname: "GE")]
    }
}
