//
//  BaseModel.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import Foundation

class BaseModel {
    var name: String?
    var isSelected: Bool = false
    
    init(name: String, isSelected: Bool = false) {
        self.name = name
        self.isSelected = isSelected
    }
}
