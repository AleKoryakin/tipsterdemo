//
//  Country.swift
//  TipsterDemo
//
//  Created by Alex on 03.02.2022.
//

import Foundation
import UIKit

class Country {
    @objc let name: String
    let code: String
    let dialCode: String!
    
    init(name: String, code: String, dialCode: String = " - ") {
        self.name = name
        self.code = code
        self.dialCode = dialCode
    }
    
    func getFlag() -> UIImage? {
        let bundle = "assets.bundle/"
        return UIImage(named: bundle + self.code.uppercased() + ".png",
                       in: Bundle(for: Self.self), compatibleWith: nil)
    }
}
