//
//  Colors.swift
//  TipsterDemo
//
//  Created by Alex on 19.01.2022.
//

import Foundation
import UIKit

struct DefaultColor {
    static let primaryDarkGray = UIColor(hex: 0x1A0F27)
    static let primaryGray = UIColor(hex: 0x271D34)
    static let primaryLightGray = UIColor(hex: 0x41394E)
    static let errorRed = UIColor(hex: 0xF8274D)
    static let primaryBlue = UIColor(hex: 0x37AAFD)
    static let tabBarGray = UIColor(hex: 0xF5F2F7)
    static let buttonInactiveGray = UIColor(hex: 0x8F8D9C)
    static let placeholderGray = UIColor(hex: 0x9C9BA9)
    static let actionSheetGray = UIColor(hex: 0x342B41)
    static let lightLabel = UIColor(hex: 0xEAEFF7)
    static let buttonGreen = UIColor(hex: 0x13AD4B)
    static let buttonYellowLight = UIColor(hex: 0xFDF266)
    static let buttonYellow = UIColor(hex: 0xF3AF3D)
    static let buttonBlackTitle = UIColor(hex: 0x42350C)
}
