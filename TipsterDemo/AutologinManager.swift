//
//  AutologinManager.swift
//  TipsterDemo
//
//  Created by Alex on 15.02.2022.
//

import Foundation

class AutologinManager {
    static let isAuthorizedKey = "isAuthorizedKey"
    
    static var isAuthorized: Bool {
        return UserDefaults.standard.bool(forKey: isAuthorizedKey)
    }
    
    static func authorize() {
        UserDefaults.standard.set(true, forKey: isAuthorizedKey)
    }
}
