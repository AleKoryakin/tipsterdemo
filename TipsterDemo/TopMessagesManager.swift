//
//  TopMessagesManager.swift
//  TipsterDemo
//
//  Created by Alex on 04.02.2022.
//

import Foundation
import UIKit

func topMostViewController() -> UIViewController? {
    let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
    
    if var topController = keyWindow?.rootViewController {
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
        return topController
    } else {
        return nil
    }
}

class TopMessagesManager {
    
    private var topView: TopErrorView = .fromNib()
    private var topConstraint: NSLayoutConstraint!
    private var timer = Timer()
    private var parrentView: UIView!
    private var defaultTopSpace: CGFloat = 60
    
    func showTopMessage(message: String) {
        guard let view = topMostViewController()?.view else { return }
        
        parrentView = view
        addMessageView()
        topView.configure(message: message)
        topView.layoutIfNeeded()
        addGestureRecognizer()
        timer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(close), userInfo: nil, repeats: false)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.animateTopConstraint(constant: self.defaultTopSpace)
        }
    }
    
    // MARK: - Actions
    
    @objc private func close() {
        animateTopConstraint(constant: -defaultTopSpace, isClose: true)
        stopTimer()
    }
    
    // MARK: - Helpers
    
    private func addMessageView() {
        topView.translatesAutoresizingMaskIntoConstraints = false
        parrentView.addSubview(topView)
        ConstraintManager.add(height: 48, width: nil, to: topView)
        topConstraint = topView.topAnchor.constraint(equalTo: parrentView.topAnchor, constant: -defaultTopSpace)
        parrentView.addConstraint(topConstraint)
        ConstraintManager.add(view: topView, to: parrentView, edge: .leading, constant: 16)
        ConstraintManager.add(view: topView, to: parrentView, edge: .trailing, constant: -16)
    }
    
    private func addGestureRecognizer() {
        let gr = UITapGestureRecognizer(target: self, action: #selector(close))
        topView.addGestureRecognizer(gr)
    }
    
    private func animateTopConstraint(constant: CGFloat, isClose: Bool = false) {
        self.topConstraint.constant = constant

        UIView.animate(withDuration: 0.3) {
            self.parrentView.layoutIfNeeded()
        } completion: { _ in
            if isClose {
                self.topView.removeFromSuperview()
            }
        }

    }
    
    private func stopTimer() {
        timer.invalidate()
    }
}
